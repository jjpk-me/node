FROM docker.io/library/node:16

WORKDIR /home/node
USER node

COPY --chown=node package.json package-lock.json /home/node/
RUN npm install
COPY --chown=node .babelrc gulpfile.js /home/node/
COPY --chown=node fonts /home/node/fonts/
COPY --chown=node img /home/node/img/
COPY --chown=node src /home/node/src/
RUN npm run build


FROM docker.io/library/nginx:alpine

COPY --from=0 /home/node/dist /usr/share/nginx/html