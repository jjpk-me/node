# jjpk.me, frontend

This repository brings together the various JS libraries I use on my personal
website, along with a few additional lines of code.

## Thanks

- [Bulma](https://github.com/jgthms/bulma), a very pleasant Flexbox CSS 
framework.
- [SimpleMDE](https://github.com/sparksuite/simplemde-markdown-editor), a 
JavaScript markdown editor (for article edition).
- [FontAwesome](https://github.com/FortAwesome/Font-Awesome), for the icons.
- [The Library 3 AM font](https://www.dafont.com/library-3-am.font), used for 
the site title.
- [The hack font](https://github.com/source-foundry/Hack), used for the site 
intro (and my entire desktop).
- The unbelievable number of dependencies without which no modern frontend 
project is complete.
