/* jjpk-me/node
 *
 * Copyright (C) 2020 - Julien JPK
 * This file is part of the frontend code for my personal website at jjpk.me.
 *
 * There is very little value in granting permissions over such a specific code
 * base. It is therefore published with no license. All rights reserved. */

const del = require('del');
const gulp = require('gulp');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const sourcemaps = require('gulp-sourcemaps');
const log = require('gulplog');
const babelify = require('babelify');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass')(require('sass'));
const uglifycss = require('gulp-uglifycss');
const rename = require('gulp-rename');

function clean() {
    return del('dist/');
}

function js() {
    const b = browserify({
        entries: 'src/js/index.js',
        debug: true
    });
    
    return b.transform(babelify)
        .bundle()
        .pipe(source('jjpk.min.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', log.error)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/js'));
}

function scss() {
    return gulp.src('src/scss/index.scss')
        .pipe(sass())
        .pipe(uglifycss())
        .pipe(rename('jjpk.min.css'))
        .pipe(gulp.dest('dist/css'));
}

function img() {
    return gulp.src('img/*')
        .pipe(gulp.dest('dist/img'));
}

function fonts() {
    return gulp.src([
        'fonts/*',
        'node_modules/@fortawesome/fontawesome-free/webfonts/*'])
        .pipe(gulp.dest('dist/fonts'));
}

exports.clean = clean;
exports.build = gulp.series(clean, gulp.parallel(js, scss, img, fonts));
exports.watch = function() {
    gulp.watch('src/js/*.js', {'ignoreInitial': false}, js);
    gulp.watch('src/scss/*.scss', {'ignoreInitial': false}, scss);
    gulp.watch('img/*', {'ignoreInitial': false}, img);
    gulp.watch('fonts/*', {'ignoreInitial': false}, fonts);
};
exports.default = exports.watch;
